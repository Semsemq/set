#!/bin/bash

# Exit immediately if a command exits with a non-zero status
set -e
# this function will test if a file exists and checks if it's a valid elf.

export LANG=C.UTF-8


Help() {

    cat <<EOF > $AUTOPKGTEST_TMP/se_update
#!/usr/bin/expect
spawn /usr/bin/setoolkit
expect "Do you agree to the terms of service \[y/n\]:"
send "y\n"
expect "set>"
send "6\n"
expect "Press <return> to continue"
send "\n"
expect "set>"
send "exit\n"
EOF

        chmod 755 $AUTOPKGTEST_TMP/se_update
        $AUTOPKGTEST_TMP/se_update

}

Check_version() {

upstream_version="$(dpkg-query -f'${source:Upstream-Version}' -W set | awk -F + '{ print $1 }')"
core_version="$(cat /usr/share/set/src/core/set.version)"
test "$upstream_version" = "$core_version"

}

Update_the_Social-Engineer_Toolkit() {

    cat <<EOF > $AUTOPKGTEST_TMP/se_update
#!/usr/bin/expect
spawn /usr/bin/setoolkit
expect "set>"
send "4\n"
expect "set>"
send "exit\n"
EOF

        chmod 755 $AUTOPKGTEST_TMP/se_update
        $AUTOPKGTEST_TMP/se_update

}

Test_Website_Attack_Vectors() {

        # Restart apache2
        #service apache2 stop
        # Restart postgresql
#        kill -9 $(lsof -t -i:80)
        service postgresql restart
        cat <<EOF > $AUTOPKGTEST_TMP/website_attacks
#!/usr/bin/expect
#set apache_running "\[!\] Apache may be running, do you want SET to stop the process? \[y/n\]:"
spawn /usr/bin/setoolkit
expect "set>"
send "1\n"
expect "set>"
send "2\n"
expect "set:webattack>"
send "2\n"
expect "set:webattack>"
expect "set:webattack>"
send "2\n"
expect "set> Are you using NAT/Port Forwarding \[yes|no\]:"
send "no\n"
expect "set:webattack> IP address or hostname for the reverse connection:"
send "127.0.0.1\n"
expect "set:webattack> Enter the url to clone:"
send "www.offensive-security.com\n"
expect "set:payloads>"
send "1\n"
expect "set:payloads>"
send "1\n"
expect "set:payloads> Port to use for the reverse \[443\]:"
send "443\n"
expect "It is currently listening"
send "\r"
expect "set:webattack>"
send "exit\n"
EOF

        chmod 755 $AUTOPKGTEST_TMP/website_attacks
        $AUTOPKGTEST_TMP/website_attacks
        #sleep 20
        #check_cloned_website

}

QRCode_Generator_Attack_Vector(){

        cat <<EOF > $AUTOPKGTEST_TMP/qrcode_attack
#!/usr/bin/expect
spawn /usr/bin/setoolkit
expect "set>"
send "1\n"
expect "set>"
send "8\n"
expect "Enter the URL you want the QRCode to go to (99 to exit):"
send "http://www.offensive-security.com\n"
expect "Press <return> to continue"
send "\r"
expect "set>"
send "99\n"
expect "set>"
send "exit"
EOF

        chmod 755 $AUTOPKGTEST_TMP/qrcode_attack
        $AUTOPKGTEST_TMP/qrcode_attack

}

###################################
# Main
###################################

email_title="$(date +%s | sha256sum | base64 | head -c 32)"
echo $email_title
for function in "$@"; do
        $function
done
